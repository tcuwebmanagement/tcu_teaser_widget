<?php
/**
 * Update plugin
 *
 * @package tcu_teaser_widget
 * @since 2.3.0
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Let's connect to our API so we can update the plugin
 */
class Tcu_Teaser_Widget_Update {

	/**
	 * API URL
	 *
	 * @var string The URL to connect to API
	 */
	const URL = 'https://webmanage.tcu.edu/api/';

	/**
	 * Plugin slug
	 *
	 * @var string Plugin slug
	 */
	const SLUG = 'tcu_teaser_widget';

	/**
	 * Class constructor
	 */
	public function __construct() {

		// Take over the update check.
		add_filter( 'pre_set_site_transient_update_plugins', array( $this, 'check_for_update' ) );

		// Take over the Plugin info screen.
		add_filter( 'plugins_api', array( $this, 'api_call' ), 10, 3 );
	}

	/**
	 * Check if a new update has been released
	 *
	 * @param mixed $transient Value of site transient. Expected to not be SQL-escaped.
	 *
	 * @return $checked_data array
	 */
	public function check_for_update( $transient ) {

		// Comment out these two lines during testing.
		if ( empty( $transient->checked ) ) {
			return $transient;
		}

		$args = array(
			'slug'    => self::SLUG,
			'version' => $transient->checked[ self::SLUG . '/' . self::SLUG . '.php' ],
		);

		$request_string = array(
			'body'       => array(
				'action'  => 'basic_check',
				'request' => serialize( $args ),
				'api-key' => md5( get_bloginfo( 'url' ) ),
			),
			'user-agent' => 'WordPress/' . TCU_Register_Teaser_Widget::get_version() . '; ' . get_bloginfo( 'url' ),
		);

		// Start checking for an update.
		$raw_response = wp_remote_post( self::URL, $request_string );

		if ( ! is_wp_error( $raw_response ) && ( 200 === $raw_response['response']['code'] ) ) {
			$response = unserialize( $raw_response['body'] );
		}

		if ( is_object( $response ) && ! empty( $response ) ) {
			// Feed the update data into WP updater.
			$transient->response[ self::SLUG . '/' . self::SLUG . '.php' ] = $response;
		}

		return $transient;
	}


	/**
	 * Take over the Plugin info screen
	 *
	 * @param array $results The result object or array. Default false.
	 * @param array $action API action to perform: 'query_plugins', 'plugin_information'.
	 * @param array $args Array or object of arguments to serialize for the Plugin Info API.
	 */
	public function api_call( $results, $action, $args ) {

		if ( ! isset( $args->slug ) || ( self::SLUG !== $args->slug ) ) {
			return $results;
		}

		// Get the current version.
		$plugin_info     = get_site_transient( 'update_plugins' );
		$current_version = $plugin_info->checked[ self::SLUG . '/' . self::SLUG . '.php' ];
		$args->version   = $current_version;

		$request_string = array(
			'body'       => array(
				'action'  => $action,
				'request' => serialize( $args ),
				'api-key' => md5( get_bloginfo( 'url' ) ),
			),
			'user-agent' => 'WordPress/' . TCU_Register_Teaser_Widget::get_version() . '; ' . get_bloginfo( 'url' ),
		);

		$request = wp_remote_post( self::URL, $request_string );

		if ( is_wp_error( $request ) ) {
			$results = new WP_Error( 'plugins_api_failed', __( 'An Unexpected HTTP Error occurred during the API request.</p> <p><a href="?" onclick="document.location.reload(); return false;">Try again</a>', 'tcu_teaser_widget' ), $request->get_error_message() );
		} else {
			$results = unserialize( $request['body'] );

			if ( false === $results ) {
				$results = new WP_Error( 'plugins_api_failed', __( 'An unknown error occurred', 'tcu_teaser_widget' ), $request['body'] );
			}
		}

		return $results;
	}
}
