<?php
/**
 * Plugin Name: TCU Teaser Widget
 * Description: This plugin gives you the ability to add a teaser modal into any widgitized area.
 * Version: 2.3.0
 * Author: Website & Social Media Management
 * Author URI: http://mkc.tcu.edu/web-management.asp
 *
 * License: GNU General Public License v2.0
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @package tcu_teaser_widget
 * @since TCU Teaser Widget 1.0.0
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Let's get started!
 */
if ( class_exists( 'TCU_Register_Teaser_Widget' ) ) {
	new TCU_Register_Teaser_Widget();
}

/**
 * Registers plugin and loads functionality
 *
 * @author Mayra Perales <m.j.perales@tcu.edu>
 */
class TCU_Register_Teaser_Widget {

	/**
	 * Plugin version
	 *
	 * @var string Plugin version
	 */
	private static $version = '2.3.0';

	/**
	 * Instace of TCU_Teaser_Widget class
	 *
	 * @var object The TCU_Teaser_Widget() object
	 */
	private $widget;

	/**
	 * Instance of Tcu_Teaser_Widget_Update class
	 *
	 * @var object The Tcu_Teaser_Widget_Update object
	 */
	protected $update;

	/**
	 * Constructor
	 */
	public function __construct() {

		// Load all our includes.
		$this->load_files();

		// Instatiate TCU_Teaser_Widget.
		$this->create_teaser();

		// Update our plugin.
		$this->instantiate_update_class();

		// Activation and uninstall hooks.
		register_activation_hook( __FILE__, array( $this, 'activate_teaser' ) );
		register_deactivation_hook( __FILE__, array( $this, 'uninstall_teaser' ) );
	}

	/**
	 * Return plugin version
	 *
	 * @return $version string Plugin version
	 */
	public static function get_version() {
		return self::$version;
	}

	/**
	 * Instantiate TCU_Teaser_Widget
	 *
	 * @return $widget object Accordion Custom Post Type
	 **/
	public function create_teaser() {
		if ( ! $this->widget ) {
			$this->widget = new TCU_Teaser_Widget();
		}
		return $this->widget;
	}

	/**
	 * Instantiate class
	 */
	public function instantiate_update_class() {
		if ( ! $this->update ) {
			$this->update = new Tcu_Teaser_Widget_Update();
		}
		return $this->update;
	}

	/**
	 * Check if TCU Web Standards theme is active
	 */
	private function check_theme() {

		// Get the current theme.
		$theme = wp_get_theme();

		// Check if the current/parent theme is TCU Web Standards.
		if ( ( 'TCU Web Standards' === $theme->name ) || ( 'TCU Web Standards' === $theme->parent_theme ) ) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Activate plugin
	 *
	 * @return bool false
	 */
	public function activate_teaser() {

		// Deactivate plugin if TCU Web Standard Theme is not installed.
		if ( ! $this->check_theme() ) {
			deactivate_plugins( plugin_basename( __FILE__ ) );
			wp_die( __( sprintf( 'Sorry, but your theme does not support this plugin. Please install the TCU Web Standards Theme.' ), 'tcu_teaser_widget' ) );
			return false;
		}

		flush_rewrite_rules();
	}

	/**
	 * Uninstall plugin and flush rewrites
	 */
	public function uninstall_teaser() {

		// Flush our rewrites.
		flush_rewrite_rules();
	}

	/**
	 * Load dependencies
	 */
	public function load_files() {
		require_once 'classes/class-tcu-teaser-widget.php';
		require_once 'classes/class-tcu-teaser-widget-update.php';
	}
}
