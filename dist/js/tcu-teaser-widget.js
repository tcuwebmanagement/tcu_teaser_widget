/**
 * Media Upload
 *
 */
jQuery(document).ready(function($) {
    // variables
    var fileFrame;

    $(document).on('click', '.tcu-teaser-widget-upload', function(e) {
        e.preventDefault();

        var button = $(this);
        var imgContainer = $(this).parent('.image-container');
        var hiddenInput = $(this).parent('.image-container').find('.tcu-teaser-widget-url').attr('id');

        // crate a new media fileFrame
        fileFrame = wp.media({
            title: 'Select Image',
            button: {
                text: 'Insert into Widget'
            },
            multiple: false // Set to true to allow multiple files to be selected
        });

        fileFrame.on('select', function() {
            var attachment = fileFrame.state().get('selection').first().toJSON();

            // Send the attachment URL to our teaser image input field
            imgContainer.prepend(
                '<img style="height: auto; max-width: 400px; width: 100%;" src="' +
                    attachment.url +
                    '" class="tcu-teaser-image" /><button type="button" class="tcu-teaser-widget-clear-image button button-secondary button-small">Remove Image</button>'
            );

            $('#' + hiddenInput).val(attachment.url);

            // hide upload button
            button.css('display', 'none');
        });

        // open the modal on click
        fileFrame.open();
    });

    // empty values for image_url, width, and height
    $(document).on('click', '.tcu-teaser-widget-clear-image', function(e) {
        e.preventDefault();

        var hiddenInput = $(this).parent('.image-container').find('.tcu-teaser-widget-url').attr('id');
        var ourImage = $(this).parent('.image-container').find('.tcu-teaser-image');
        var imgContainer = $(this).parent('.image-container');
        var removeImage = $(this);

        ourImage.remove();
        imgContainer.prepend(
            '<button type="button" class="tcu-teaser-widget-upload button button-primary">Upload Image</button>'
        );
        removeImage.css('display', 'none');
        $('#' + hiddenInput).val('');
    });
});
