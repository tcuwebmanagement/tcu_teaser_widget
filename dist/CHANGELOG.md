## TCU Teaser Widget

Author: Mayra Perales <mailto:m.j.perales@tcu.edu>

---

**v2.3.0**

*   Add ESLint, SassLint, and PHPCS for WP code standards
*   Refactor code that was repeated
*   Fix accessibility issues by removing section tag
*   Add conditional for image and aria-label

**v2.2.2**

*   Moved update classes into a PHP Class
*   Added aria-label to hyperlink tag
*   Added alt attribute to image tag
*   Added height and width to SVG arrow icon

**v2.1.2**

*   Replaced the Auto-Update class
*   Added md5 hash to mask the update URL

**v1.1.2**

*   Added auto update class

**v1.0.2**

*   inlined styles

**v1.0.1**

*   removed .tcu-mar-t0 .tcu-line-height-1 from title

**v1.0.0**

*   Initial theme
