<?php
/**
 * Our widget class
 *
 * @package tcu_teaser_widget
 * @since TCU Teaser Widget 2.3.0
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

// Load our widget.
add_action(
	'widgets_init', function () {
		register_widget( 'TCU_Teaser_Widget' );
	}
);

/**
 *  Our Teaser Widget class
 */
class TCU_Teaser_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress
	 */
	public function __construct() {
		parent::__construct(
			'tcu_teaser_widget',
			__( 'Teaser Module' ),
			array( 'description' => __( 'Add a teaser module to any widgitized area', 'tcu_teaser_widget' ) )
		);

		add_action( 'sidebar_admin_setup', array( $this, 'upload_scripts' ) );
	}

	/**
	 * Enqueue all the javascript.
	 */
	public function upload_scripts() {
		wp_enqueue_media();
		wp_enqueue_script( 'tcu-teaser-widget', plugins_url( 'js/tcu-teaser-widget.js', dirname( __FILE__ ) ), array( 'jquery', 'media-upload', 'media-views' ), '', true );
	}

	/**
	 * Front-end display of widget
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		global $post;

		// These are the widget options.
		$title    = $instance['title'];
		$image    = $instance['image'];
		$alt      = $instance['alt'];
		$url      = $instance['url'];
		$desc     = $instance['desc'];
		$textarea = $instance['textarea'];

		// Before widget (defined by themes).
		echo wp_kses_post( $args['before_widget'] );
		?>
		<div class="tcu-modal cf">
			<?php if( $image ) : ?>
				<img alt="<?php echo esc_attr( $alt ); ?>" src="<?php echo esc_url( $image ); ?>" />
			<?php endif; ?>

			<div class="tcu-modal__content">
				<h3 class="tcu-uppercase"><?php echo wp_kses_post( $title ); ?></h3>
				<?php echo esc_textarea( $textarea ); ?>
			</div>

			<a <?php if ( $desc ) { echo 'aria-label="' . esc_attr( $desc ) . '"'; } ?> class="tcu-button tcu-button--primary tcu-bounce tcu-bounce--right--grey tcu-full-width" href="<?php echo esc_url( $url ); ?>">
				Read More
				<svg height="30" width="30"><use xlink:href="#play-icon"></use></svg>
			</a>
		</div><!-- end of .tcu-modal -->

		<?php

		// After widget (defined by themes).
		echo wp_kses_post( $args['after_widget'] );
	}

	/**
	 * Form UI
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {


		$title     = ( $instance ) ? $instance['title'] : '' ;
		$alt       = ( $instance ) ? $instance['alt'] : '';
		$image_url = ( $instance ) ? $instance['image'] : '';
		$url       = ( $instance ) ? $instance['url'] : '';
		$desc      = ( $instance ) ? $instance['desc'] : '';
		$textarea  = ( $instance ) ? $instance['textarea'] : '';
		$open      = '<p>';
		$closing   = '</p>';
		?>

		<div class="tcu-teaser-widget-container">

			<div style="text-align: center; margin-top: 1em; margin-bottom: 1em;" class="image-container">

				<?php
				if ( $image_url ) {
					echo '<img style="height: auto; max-width: 400px; width: 100%;" class="tcu-teaser-image" src="' . esc_url( $image_url ) . '" />
					<button type="button" class="tcu-teaser-widget-clear-image button button-secondary button-small">Remove Image</button>';
				} else {
					echo '<button type="button" class="tcu-teaser-widget-upload button button-primary">Upload Image</button>';
				}
				echo $this->create_input( 'hidden',  $this->get_field_id( 'image' ), $this->get_field_name( 'image' ), $image_url, 'tcu-teaser-widget-url' );
				?>

			</div><!-- end of .image-container -->


			<?php
			// Alt.
			echo $open;
			echo $this->create_label( $this->get_field_id( 'alt' ), 'Alt Text for image:' );
			echo $this->create_input( 'text', $this->get_field_id( 'alt' ), $this->get_field_name( 'alt' ), $alt );
			echo $closing;

			// Title.
			echo $open;
			echo $this->create_label( $this->get_field_id( 'title' ), 'Title:' );
			echo $this->create_input( 'text', $this->get_field_id( 'title' ), $this->get_field_name( 'title' ), $title );
			echo $closing;

			// Content.
			echo $open;
			echo $this->create_label( $this->get_field_id( 'textarea' ), 'Content:' );
			echo $this->create_input( 'textarea', $this->get_field_id( 'textarea' ), $this->get_field_name( 'textarea' ), $textarea );
			echo $closing;

			// LINK.
			echo $open;
			echo $this->create_label( $this->get_field_id( 'url' ), 'Link:' );
			echo $this->create_input( 'url', $this->get_field_id( 'url' ), $this->get_field_name( 'url' ), $url );
			echo $closing;

			// Aria Label.
			echo $open;
			echo $this->create_label( $this->get_field_id( 'desc' ), 'Aria-label:' );
			echo $this->create_input( 'text', $this->get_field_id( 'desc' ), $this->get_field_name( 'desc' ), $desc );
			?>

			<span style="font-size: 10px; font-style: italic;">
				<?php esc_html_e( 'The objective is to describe the purpose of the link. Example: Read more about Seminole\'s new baby mayor.', 'tcu_teaser_widget' ); ?>
				<a target="_blank" href="https://www.w3.org/TR/WCAG20-TECHS/ARIA8/">More Information</a>
			</span>
			<?php echo $closing; ?>

		</div><!-- end of .tcu-teaser-widget-container -->

	<?php
	}

	/**
	 * Sanitize widget form values as they are saved
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['title']    = strip_tags( $new_instance['title'] );
		$instance['image']    = strip_tags( $new_instance['image'] );
		$instance['alt']      = strip_tags( $new_instance['alt'] );
		$instance['url']      = strip_tags( $new_instance['url'] );
		$instance['desc']     = strip_tags( $new_instance['desc'] );
		$instance['textarea'] = strip_tags( $new_instance['textarea'] );
		return $instance;
	}

	/**
	 * Create a label input in our widget form
	 *
	 * @param string $for The for attribute for the label input.
	 * @param string $text The label title
	 */
	private function create_label( $for, $text ) {
		return '<label for="' . esc_attr( $for ) .'">'. esc_html__( $text, 'tcu_teaser_widget' ) .'</label>';
	}

	/**
	 * Create an input in our widget form
	 *
	 * @param string $type The type of input needed ('text', 'url', 'textarea')
	 * @param string $id The input id attribute.
	 * @param string $name The input name attribute.
	 * @param string $value The input value attribute.
	 * @param string $classname The classname of the input.
	 */
	private function create_input( $type, $id, $name, $value, $classname = 'widefat' ) {

		if ( $type === 'textarea') {
			return '<textarea class="' . esc_attr( $classname ) . '" name="' . esc_attr( $name ) . '" id="' . esc_attr( $id ) . '" cols="30" rows="10">' . esc_html__( $value )  . '</textarea>';
		} else {
			return '<input class="' . esc_attr( $classname ) . '" id="'. esc_attr( $id ) . '" name="'. esc_attr( $name ) .'" type="'. esc_attr( $type ). '"  value="' . esc_attr( $value ) . '">';
		}
	}
}
