<?php
/**
 * Our widget class
 *
 * @package tcu_teaser_widget
 * @since TCU Teaser Widget 1.0.0
 *
 * @deprecated See file class-tcu-teaser-widget.php
 * @deprecated
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

// Load our widget.
add_action(
	'widgets_init', function () {
		register_widget( 'TCU_Teaser_Widget' );
	}
);

/**
 *  Our Teaser Widget class
 *
 * @deprecated Since 2.2.2 No longer used by internal code and not recommended.
 */
class TCU_Teaser_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress
	 */
	public function __construct() {
		parent::__construct(
			'tcu_teaser_widget',
			__( 'Teaser Module' ),
			array( 'description' => __( 'Add a teaser module to any widgitized area', 'tcu_teaser_widget' ) )
		);

		add_action( 'sidebar_admin_setup', array( $this, 'upload_scripts' ) );
	}

	/**
	 * Enqueue all the javascript.
	 */
	public function upload_scripts() {
		wp_enqueue_media();
		wp_enqueue_script( 'tcu-teaser-widget', plugins_url( 'js/tcu-teaser-widget.js', dirname( __FILE__ ) ), array( 'jquery', 'media-upload', 'media-views' ), '', true );
	}

	/**
	 * Front-end display of widget
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		global $post;

		// These are the widget options.
		$title    = $instance['title'];
		$image    = $instance['image'];
		$alt      = $instance['alt'];
		$url      = $instance['url'];
		$desc     = $instance['desc'];
		$textarea = $instance['textarea'];

		// Before widget (defined by themes).
		echo wp_kses_post( $args['before_widget'] );
		?>
		<div class="tcu-modal cf">
			<img alt="<?php echo esc_attr( $alt ); ?>" src="<?php echo esc_url( $image ); ?>" />

			<section class="tcu-modal__content">
				<h3 class="tcu-uppercase"><?php echo wp_kses_post( $title ); ?></h3>
				<?php echo esc_textarea( $textarea ); ?>
			</section>

			<a aria-label="<?php echo esc_attr( $desc ); ?>" class="tcu-button tcu-button--primary tcu-bounce tcu-bounce--right--grey tcu-full-width" href="<?php echo esc_url( $url ); ?>">
				Read More
				<svg height="30" width="30"><use xlink:href="#play-icon"></use></svg>
			</a>
		</div><!-- end of .tcu-modal -->

		<?php

		// After widget (defined by themes).
		echo wp_kses_post( $args['after_widget'] );
	}

	/**
	 * Form UI
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {

		// Check our values.
		if ( $instance ) {
			$title     = $instance['title'];
			$image_url = $instance['image'];
			$alt       = $instance['alt'];
			$url       = $instance['url'];
			$desc      = $instance['desc'];
			$textarea  = $instance['textarea'];
		} else {
			$title     = '';
			$image_url = '';
			$alt       = '';
			$url       = '';
			$desc      = '';
			$textarea  = '';
		}
		?>

		<div class="tcu-teaser-widget-container">

			<div style="text-align: center; margin-top: 1em; margin-bottom: 1em;" class="image-container">

				<?php
				if ( $image_url ) {
					echo '<img style="height: auto; max-width: 400px; width: 100%;" class="tcu-teaser-image" src="' . esc_url( $image_url ) . '" />
					<button type="button" class="tcu-teaser-widget-clear-image button button-secondary button-small">Remove Image</button>';
				} else {
					echo '<button type="button" class="tcu-teaser-widget-upload button button-primary">Upload Image</button>';
				}
				?>

				<input class="tcu-teaser-widget-url" id="<?php echo esc_attr( $this->get_field_id( 'image' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'image' ) ); ?>" type="hidden" value="<?php echo esc_url( $image_url ); ?>">

			</div><!-- end of .image-container -->

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'alt' ) ); ?>"><?php esc_html_e( 'Alt Text for image:', 'tcu_widget_teaser' ); ?></label>
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'alt' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'alt' ) ); ?>" type="text" value="<?php echo esc_attr( $alt ); ?>">
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'tcu_teaser_widget' ); ?></label>
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'textarea' ) ); ?>"><?php esc_html_e( 'Content:' ); ?></label>
				<textarea class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'textarea' ) ); ?>" id="<?php echo esc_attr( $this->get_field_id( 'textarea' ) ); ?>" cols="30" rows="10"><?php echo esc_attr( $textarea ); ?></textarea>

			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'url' ) ); ?>"><?php esc_html_e( 'Link:', 'tcu_teaser_widget' ); ?></label>
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'url' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'url' ) ); ?>" type="url" value="<?php echo esc_url( $url ); ?>">
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'desc' ) ); ?>"><?php esc_html_e( 'Link Description:', 'tcu_teaser_widget' ); ?></label>
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'desc' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'desc' ) ); ?>" type="text" value="<?php echo esc_attr( $desc ); ?>">
				<span style="font-size: 10px; font-style: italic;">
					<?php esc_html_e( 'The objective is to describe the purpose of the link. Example: Read more about Seminole\'s new baby mayor.', 'tcu_teaser_widget' ); ?>
					<a target="_blank" href="https://www.w3.org/TR/WCAG20-TECHS/ARIA8">More Information</a>
				</span>
			</p>

		</div><!-- end of .tcu-teaser-widget-container -->

	<?php
	}

	/**
	 * Sanitize widget form values as they are saved
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['title']    = strip_tags( $new_instance['title'] );
		$instance['image']    = strip_tags( $new_instance['image'] );
		$instance['alt']      = strip_tags( $new_instance['alt'] );
		$instance['url']      = strip_tags( $new_instance['url'] );
		$instance['desc']     = strip_tags( $new_instance['desc'] );
		$instance['textarea'] = strip_tags( $new_instance['textarea'] );
		return $instance;
	}
}
