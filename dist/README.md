# TCU Teaser Widget Plugin

Use the TCU Teaser Widget Plugin to easily add a teaser module to any widget area. This plugin must include TCU Web Standards theme.

# Development

**GETTING STARTED**

We use GRUNT as our task runner. All our WordPress production files are located in `dist` directory. When you are ready to ship the next plugin version, copy files into the dist directy by running `grunt copy`. Update the version number in `Gruntfile.js zip` task and run `grunt zip` to compress a clean copy of the next plugin version.

`grunt build` Copy clean (without hidden files like Git, .eslintrc and so on) production ready files into the `dist` directory.

**Our Files**

We offer two versions — a minified version, and an un-minified one. Use the minified version in a production environment or to reduce the file size of your downloaded assets. And the un-minified version is better if you are in a development environment or would like to debug the CSS or JavaScript assets in the browser.

**Our main SASS files are**

    library/scss

**WordPress Plugin**

Run `grunt zip` to compress a clean zip version of the `dist` directory. From the WordPress admin screen, select `Plugins > Add New` from the menu. Click on Add new and select the zip file you just created to install. Finally, activate the plugin.

    dist/

**Working with an existing Grunt project**

If you haven't used Grunt before, be sure to check out the **[Getting Started Guide](http://gruntjs.com/getting-started)**, as it explains how to create a **[Gruntfile](http://gruntjs.com/sample-gruntfile)** as well as install and use Grunt plugins. Assuming that the `Grunt CLI` has been installed and that the project has already been configured with a package.json and a Gruntfile, it's very easy to start working with Grunt.

*   Change to the project's root directory `cd project/path/`
*   Install project dependencies with `npm install`.
*   Compress project with Grunt with `grunt zip`.

**Grunt Tasks**

`grunt zip` Compress the `dist` directory into `tcu_web_standards` folder and name the zip file `tcu_web_standards.VERSION.NUM.zip`

**Install NPM**

**[Download NPM](https://www.npmjs.com/)**

Developed by **Mayra Perales**: <mailto:m.j.perales@tcu.edu>

## Special thanks to:

    - Eddie Machado - http://themble.com/bones
    - Paul Irish & the HTML5 Boilerplate
    - Yoast for some WP functions & optimization ideas
    - Andrew Rogers for code optimization
    - David Dellanave for speed & code optimization
    - and several other developers. :)

## Submit Bugs & or Fixes:

_[Create an issue](https://bitbucket.org/TCUWebmanage/tcu_teaser_widget/issues?status=new&status=open)_

## Meta

*   [Changelog](CHANGELOG.md)
