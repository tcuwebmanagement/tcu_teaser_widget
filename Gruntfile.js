/*eslint no-undef: "error"*/
/*eslint-env node*/

module.exports = function( grunt ) {
    grunt.initConfig( {

        // Let's minimize our JS files
        uglify: {

            // Front-end
            main: {
                src: 'js/tcu-teaser-widget.js',
                dest: 'js/tcu-teaser-widget.min.js'
            }
        },

        // This creates a clean WP plugin copy to use in production
        copy: {
            main: {
                files: [
                    {
                        expand: true,
                        src: [
                            './*.php',
                            './CHANGELOG.md',
                            './README.md',
                            'classes/*.php',
                            'css/**.css',
                            'js/**.js',
                            'css/!*.map'
                        ],
                        dest: 'dist/'
                    }
                ]
            }
        },

        // Let's zip up our /dist (production wordpress plugin)
        // Change version number in tcu_downloads.php
        compress: {
            main: {
                options: {
                    archive: 'tcu_teaser_widget.2.3.0.zip'
                },
                files: [
                    {
                        expand: true,
                        cwd: 'dist/',
                        src: [ '**' ],
                        dest: 'tcu_teaser_widget/'
                    }
                ]
            }
        }
    } );

    grunt.loadNpmTasks( 'grunt-contrib-uglify' );
    grunt.loadNpmTasks( 'grunt-contrib-copy' );
    grunt.loadNpmTasks( 'grunt-contrib-compress' );

    // default
    grunt.registerTask( 'default', [ 'uglify' ] );

    // Copy to dist/
    grunt.registerTask( 'build', [ 'copy' ] );

    // Zip our clean directory to easily install in WP
    grunt.registerTask( 'zip', [ 'compress' ] );
};
